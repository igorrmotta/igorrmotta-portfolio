Rails.application.routes.draw do
  root to: 'static_pages#home'

  # match '/contacts', to: 'contacts#new', via: :get
  # match '/contacts', to: 'contacts#create', via: :post

  match '/contacts', to: 'static_pages#create_contact', via: :post

  match '/home', to: 'static_pages#home', via: :get
  match '/about', to: 'static_pages#about', via: :get
  match '/contact', to: 'static_pages#contact', via: :get
  match '/portfolio', to: 'static_pages#portfolio', via: :get

  #match ':controller(/:action(/:id))', via: :get
  #match ':controller(/:action(/:id(.:format)))', via: :get

end
