# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

Rails.application.config.assets.precompile += %w( jquery.js )
Rails.application.config.assets.precompile += %w( jquery.poptrox.min.js )
Rails.application.config.assets.precompile += %w( skel.min.js )
Rails.application.config.assets.precompile += %w( ie/respond.min.js )
Rails.application.config.assets.precompile += %w( ie/html5shiv.js )


Rails.application.config.assets.precompile += %w( thumbs/01.jpg )
Rails.application.config.assets.precompile += %w( thumbs/02.jpg )
Rails.application.config.assets.precompile += %w( thumbs/avatar.jpg )


Rails.application.config.assets.precompile += %w( fulls/01.jpg )
Rails.application.config.assets.precompile += %w( fulls/02.jpg )
Rails.application.config.assets.precompile += %w( fulls/avatar.jpg )