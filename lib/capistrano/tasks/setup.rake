namespace :setup do
  desc "Symlink shared config files"
  task :symlink_config_files do
    on roles(:app) do
      run "#{sudo} ln -s #{ deploy_to }/shared/config/database.yml #{ current_path }/config/database.yml"
    end
  end

  desc "Restart Passenger app"
  task :restart do
    on roles(:app) do
      run "#{sudo} touch #{ File.join(current_path, 'tmp', 'restart.txt') }"
    end
  end
end