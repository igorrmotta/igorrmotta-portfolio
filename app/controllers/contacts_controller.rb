class ContactsController < ApplicationController
  def show
    @contact
  end

  def new
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params_with_no_date)
  end

  private
  def contact_params_with_no_date
    params.require(:contact).permit(:name, :email, :message)
  end
end
