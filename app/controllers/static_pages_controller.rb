class StaticPagesController < ApplicationController
  def home
    @contact = Contact.new
  end

  def about
  end

  def portfolio
  end

  def contact
    @contact = Contact.new
  end

  def create_contact
    @contact = Contact.new(contact_params_with_no_date)
    if (@contact.save!)
      flash[:success] = 'Thanks for your contact. You will get in touch soon.'
    else
      flash[:danger] = 'Ooops!'
    end

    redirect_to contact_path
  end

  private
  def contact_params_with_no_date
    params.require(:contact).permit(:name, :email, :message)
  end
end
